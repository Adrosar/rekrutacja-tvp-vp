/**
 * Obiekt klasy 'UpdaterClass' jest lepszym zamiennikiem funkcji 'setInterval' i 'clearInterval'.
 */
export class UpdaterClass {

    private fn: Function;
    private timeout: number;
    private id: number|null;

    /**
     * Konstruktor klasy.
     * @param fn: Funkcja która będzie wykonywana co określony czas.
     * @param timeout: Czas oczekiwania pomiędzy kolejnymi wykonaniami funkcji.
     */
    constructor(fn: Function, timeout: number) {
        this.fn = fn;
        this.timeout = timeout;
        this.id = null;
    }

    /**
     * Uruchamia jeden raz przekazaną funkcję.
     */
    public fire() {
        this.fn();
    }

    /**
     * Ustawia 'setInterval'.
     */
    public on() {
        if (!this.id) {
            this.id = window.setInterval(this.fn, this.timeout);
        }
    }

    /**
     * Kasuje 'clearInterval'.
     */
    public off() {
        if (!!this.id) {
            window.clearInterval(this.id);
            this.id = null;
        }
    }
}