#!/bin/bash
cd $PWD
export PATH="$PWD:$PWD/node_modules/.bin:$PATH"
export webpack="$PWD/node_modules/.bin/webpack"
$webpack --progress --optimize-minimize --optimize-occurence-order --config "$PWD/webpack.config.js"