import {ControlClass} from "./ControlClass";
import {createElementExt, appendStyle} from "./helpers";
import {createWidget, WidgetClass} from './widget';
import {UpdaterClass} from './UpdaterClass';

declare var require: any;

export class ListClass {

    private listElement: HTMLDivElement;
    private control: ControlClass;
    private updater: UpdaterClass;
    private list: Array<any>;// Lista filmów.
    private active: string;

    constructor(control: ControlClass, width: number) {
        appendStyle(require('../css/list.css'));

        this.control = control;
        this.listElement = createElementExt('div', {
            'class': 'vp-list',
            'style': {
                'width': width + 'px',
                'height': '300px'
            }
        });

        // 'this.updater' cyklicznie wykonuje dane akcje:
        // Dokładność etykiety z czasem to 1 sekunda,
        // więc pętla co 900ms wystarczy.
        this.updater = new UpdaterClass(() => {

            if (!!this.list && !!this.active) {

                let time: number = this.control.getTime();
                let duration: number = this.list[this.active].duration;

                if (time > duration) {
                    this.list[this.active].time = duration;
                    time = duration;
                }

                time = Math.floor(time);

                // Aktualizuje czas odtwarzania filmu:
                this.list[this.active].time = time;

                // Aktualizuje widok z czasem odtwarzania:
                this.list[this.active].item['labelTime']._.innerHTML = time;

                // Pilnuje odtwarzania list filmów po kolei:
                if (time == duration) {
                    // Przeskocz do następnego filmu.
                    let l: number = this.list.length;
                    let i: number = parseInt(this.active);

                    if ((i + 1) < l) {
                        this.itemEventClickFn(this.list[i + 1].item, this.list, (i + 1) + '');
                    }
                }

                // ...
                if (this.control.getStatus() == -1) {
                    this.initFirstMovies();
                }
            }


        }, 900);

    }

    public getListElement() {
        return this.listElement;
    }

    /**
     * Metoda generuje pozycje na liście odtwarzania.
     * @param list: Lista odtwarzania.
     */
    public genItems(list: Array<any>) {

        // Generuję w pętli kolejne pozycje na liście:
        for (let key in list) {
            let item: WidgetClass = createWidget(['container', 'div', {
                'class': 'vp-list-item',
                'text': '❯ '
            }, [
                ['labelName', 'span', {
                    'text': list[key].name + ' | Czas: ',
                    'class': 'vp-item-label'
                }],
                ['labelTime', 'span', {
                    'text': '0',
                    'class': 'vp-item-label'
                }],
                ['labelDuration', 'span', {
                    'text': ' / ' + list[key].duration,
                    'class': 'vp-item-label'
                }],
            ]]);

            this.itemEventClickReg(item, list, key);
            this.listElement.appendChild(item._);
            list[key].item = item;//<- przypisuje stworzony element do parametru.
            //console.log(item);
        }

        // Listę filmów wraz z stworzonymi pozycjami 'item: WidgetClass' przypisuję do parametru klasy:
        this.list = list;

        // Inicjuje pierwszy film z listy:
        this.initFirstMovies();

    }

    /**
     * Metoda przypisuje zdarzenie kliknięcia na element listy z akcjami do wykonania.
     * @param item
     * @param list
     * @param key
     */
    private itemEventClickReg(item: WidgetClass, list: Array<any>, key: string) {
        item._.addEventListener('click', () => {
            this.itemEventClickFn(item, list, key);
        });
    }

    private itemEventClickFn(item: WidgetClass, list: Array<any>, key: string) {
        this.updater.off();
        //console.log('list item click', list[key].url);

        // Stopuje i uruchamiam odtwarzanie nowego filmu:
        this.control.stop();

        // Ustawiam zaślepkę w postaci pliku graficznego:
        this.control.setPoster(this.list[key].poster);

        // Odtwarzam film od momentu na którym skończono poprzednie odtwarzanie,
        // chyba że zresetowano (zastopowano film):
        let url: string = this.list[key].url;
        if (!!this.list[key].time) {
            url += '#t=' + Math.floor(this.list[key].time);
            //console.log(url);
        }

        this.control.play(url);
        this.active = key;

        // Oznaczam odtwarzaną pozycję jako AKTYWNĄ:
        this.setActiveItem(item);

        //console.log('this.active', this.active);
        this.updater.on();
    }

    private setActiveItem(item: WidgetClass) {
        // Oznaczam wszystkie pozycje na liście jako NIE aktywne:
        for (let key in this.list) {
            this.list[key].item._.classList.remove('vp-active');
        }

        // Oznaczam odtwarzaną pozycję jako AKTYWNĄ:
        item._.classList.add('vp-active');
    }

    private initFirstMovies() {
        // Inicjuje pierwszy film z listy:
        if (!!this.list[0]) {
            //console.log('first movie', this.list[0]);
            this.control.setPoster(this.list[0].poster);// <- ładuje obrazek
            this.control.setInitURL(this.list[0].url);// <- Ustawiam startowy URL filmu
            this.setActiveItem(this.list[0].item);
            this.active = '0';
            this.updater.on();
        }
    }

}