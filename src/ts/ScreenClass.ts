import {createElementExt} from "./helpers";
import {appendStyle} from "./helpers";

declare var require: any;

/**
 * Klasa ekranu:
 * - Rysuje ekran o zadanych wymiarach.
 * - Odtwarza plik wideo.
 * - Posiada metody sterujące odtwarzaniem.
 * - Posiada metody odczytujące stan odtwarzanego materiału.
 */
export class ScreenClass {

    private screenElement: HTMLDivElement;
    private videoElement: HTMLVideoElement;
    private sourceElement: HTMLSourceElement;
    private url: string;

    private width: number;
    private height: number

    /**
     * Konstruktor klasy.
     * @param width: Szerokość obrazu w px.
     * @param height: Wysokość obrazu w px.
     */
    constructor(width: number, height: number) {
        appendStyle(require('../css/screen.css'));

        this.width = width;
        this.height = height;

        this.screenElement = createElementExt('div', {
            'class': 'vp-screen',
            'style': {
                'background-color': '#000000',
                'width': width + 'px',
                'height': height + 'px'
            }
        });
    }

    /**
     * Metoda inicjuje nowy element HTML Video i przypisuje je do parametru 'this.videoElement'
     */
    protected initVideoElement() {

        this.videoElement = createElementExt('video', {
            'preload': 'none',// auto|metadata|none
            'width': this.width,
            'height': this.height
        });

        this.screenElement.appendChild(this.videoElement);
    }

    /**
     * Metoda zatrzymuje pobieranie pliku wideo i niszczy element HTML Video i źródło:
     */
    protected destroyVideoElement() {

        // Usuwam źródło:
        this.videoElement.removeChild(this.sourceElement);

        // Zatrzymuje wczytywanie pliku wideo:
        this.videoElement.src = '';
        this.videoElement.load();

        // Usuwam element HTML Video
        this.videoElement.parentNode.removeChild(this.videoElement);

        // Czyszczę uchwyty do tych elementów:
        this.videoElement = null;
        this.sourceElement = null;
    }


    /**
     * Metoda ustawia dla elmentu HTML Video nowe źródło filmu:
     * @param src: Adres URL do pliku.
     * @param type: Typ odtwarzanego pliku.
     */
    protected setSource(src: string, type?: string|null) {
        this.sourceElement = createElementExt('source', {
            'src': src
        });

        if (!!type) {
            this.sourceElement.type = type;
        }

        this.videoElement.appendChild(this.sourceElement)
    }

    /**
     * Metoda zwraca element HTML dla obiektu klasy 'ScreenClass':
     * @returns {HTMLDivElement}
     */
    public getScreenElement() {
        return this.screenElement;
    }

    /**
     * Metoda stopuje odtwarzanie filmu:
     */
    public stop() {
        if (!!this.videoElement) {
            this.pause();
            this.destroyVideoElement();
        }
    }

    /**
     * Metoda tworzy nowy element HTML Video i ustawia adres URL jako źródło filmu:
     * @param url: Adres URL do filmu.
     */
    public load(url: string) {
        // Tworzę element VIDEO:
        this.initVideoElement();

        // Ustawiam źródło filmu:
        this.setSource(url);

        // Zapisuje adres URL:
        this.url = url;

    }

    /**
     * Metoda uruchamia lub wznawia odtwarzanie filmu.
     * @param url: Adres URL pliku który ma zostać odtworzony.
     */
    public play(url?: string|null) {

        // Ustawiam poprzedni adres URL (ten który został wcześniej zastopowany):
        if (!this.videoElement && !url && !!this.url) {
            this.load(this.url);
        }

        // Ustawiam nowy adres URL do odtwarzania:
        if (!this.videoElement && !!url) {
            this.load(url);
        }

        // Uruchamiam odtwarzanie filmu:
        if (!!this.videoElement && (this.getStatus() == 0)) {
            this.videoElement.play();
        }
    }

    /**
     * Metoda pauzuje (zatrzymuje) odtwarzanie filmu.
     */
    public pause() {
        if (!!this.videoElement) {
            this.videoElement.pause();
        }
    }

    /**
     * Metoda ustawia wskaźnik odtwarzania na podaną wartość.
     * @param time: Czas w sekundach.
     */
    public setTime(time: number) {
        this.videoElement.currentTime = time;
    }

    /**
     * Metoda zwraca obecny czas odtwarzania filmu.
     * @returns {number} Czas w sekundach.
     */
    public getTime() {
        if (!!this.videoElement) {
            return this.videoElement.currentTime;
        }

        return 0;

    }

    /**
     * Metoda zwraca czas trwania filmu.
     * @returns {number} Czas w sekundach.
     */
    public getDuration() {
        if (!!this.videoElement) {
            return this.videoElement.duration;
        }

        return 0;
    }

    public setVolume(volume: number) {
        if (!!this.videoElement) {
            this.videoElement.volume = volume;
        }
    }

    /**
     * Metoda zwraca status odtwarzania.
     * @returns {number}: -1: stopped
     *                     0: paused
     *                     1: played
     */
    public getStatus(): number {

        if (!this.videoElement) {
            return -1;// stopped
        } else {
            if (this.videoElement.paused) {
                return 0;// paused
            } else {
                return 1;// played
            }
        }

    }

    /**
     * Metoda zwraca rozmiar ekranu.
     * @returns {[number,number]}: szerokość [px], wysokość [px]
     */
    public getSize() {
        return [this.width, this.height];
    }

    /**
     * Metoda ustawia zaślepkę na ekran w postaci pliku graficznego.
     * @param url: Adres URL pliku graficznego: jpg, png.
     */
    public setPoster(url: string|null) {
        //console.log('set poster', url);

        if (!!url) {
            this.screenElement.style.backgroundImage = 'url("' + url + '")';
        } else {
            this.screenElement.style.backgroundImage = 'none';
        }
    }
}