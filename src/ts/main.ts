import {PlayerClass} from './PlayerClass';
import {getDocument} from './helpers';


if (!(<any>window).vpTVP) {

    // API:
    (<any>window).vpTVP = {
        /**
         * Funkcja umieszcza VideoPlayer w elemencie DIV.
         * @param parentID: ID elementu DIV.
         * @param width: Szerokość w px.
         * @param height: Wysokość w px.
         */
        createPlayerIn: (parentID: string, width: number, height: number, url:string) => {

            // Tworzę obiekt player'a:
            let player: PlayerClass = new PlayerClass(width, height);

            // ładuję listę filmów:
            player.loadPlayList(url);

            // Dodaję player do kontenera DIV:
            let parent: HTMLDivElement = <HTMLDivElement>(getDocument().getElementById(parentID));
            parent.appendChild(player.getPlayerElement());
        }
    };

}

