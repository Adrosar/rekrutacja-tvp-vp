## Rozpoczynamy pracę :)
... czyli przygotowanie środowiska deweloperskiego.

### Wymagane programy:
Pobierz i zainstaluj!
- Node.js https://nodejs.org
- NPM https://www.npmjs.com

### Wymagane pakiety NPM:
Otwórz konsole i przejdź do głównego katalogu projektu, a następnie wykonaj w konsoli poniższe polecenia:

1) Zainstaluj globalnie moduły:
    
    
    npm install --global typescript
    npm install --global gulp-cli
    npm install --global webpack
    npm install --global postcss-cli


2) Zainstaluj zależności dla projektu:


    npm install --dev


### Jak pracować?
Gdy pracujesz uruchom:

    webpack-watch.bat

Skrypt będzie czuwał w tle i wykonywał zadania gdy plik(i) zostaną zmienione.

Jeżeli chcesz zbudować zoptymalizowaną wersję produkcyjną użyj:

    webpack-build.bat
    
### Jak uruchomić demo?
Najprościej jest przenieść główny katalog projektu do jakiekolwiek serwera HTTP, np: Apache.

Jeżeli nie możesz tego zrobić a masz zainstalowane oprogramowanie **DOCKER** to zainteresują cię dwa skrypty:

1) Uruchamia kontener z serwerem Apche 2.4:


    ./docker-run.sh
   
   
2) Zatrzymuje i kasuje kontener z serwerem:


    ./docker-kill.sh
   
   
Uruchomiony serwer jest dostępny pod adresem lokalnym 127.0.0.1:80 (Linux) lub pod adresem maszyny wirtualnej docker x.x.x.x:80 (Windows, MacOS)
Teraz tylko wystarczy przejść pod adres **http://127.0.0.1:80/demo**

Adres mazyny wirtualnej sprawdzamy poleceniem:

    docker-machine ls

**!!! UWAGA !!!** Jeżeli używasz system **Windows** lub **Mac OS** to projekt musi znajdować się w katalogu użytkownika (może być zagłębiony w innym katalogu). Chodzi o to że **DOCKER** na tych dwuch platformach ma dostęp tylko do katalogu użytkownika! 


### Pliki i foldery

1. Plik __tsconfig.json__ zawiera konfigurację dla język TypeScript

2. Plik **webpack.config.js** zawiera konfigurację dla modułu **Webpack**

3. Plik **postcss.config.js** zawiera konfigurację modułu **postcss** który przetwarza style CSS

4. Plik **package.json** zawiera konfigurację pakietu **NPM**, czyli wszystkie wymagane moduły do poprawnego działania środowiska deweloperskiego

5. Folder **demo** zawiera przykład wykorzystania wtyczki 'dist/videoplayer.js'

6. Pliki wideo z katalogu **/demo/movies/** należy pobrać z poniższego linku:

    - LINK: https://drive.google.com/file/d/0BxVsrriiFTq1OS15VFZaTVZTQUE/view?usp=sharing
    - PLIK: rekrutacja-tvp-vp-81002343.7z
    - HASŁO: bPl6Rmw49W]ih!J`<QGSJvH9{~x^C~Zq
    - MD5: B44906235B9337C69406669037586A92
    - SHA1: 9F41A0735849DC01DA74A2C73B6E1C11FB0D60D3
    - CRC32: 81002343


### Kompatybilność projektu
Przetestowane na `system / przeglądarka`:

- `Windows 8.1 / Opera 42.0.2393.137`
- `Windows 8.1 / Google Chrome 55.0.2883.87 m`
- `Windows 8.1 / Firefox 51.0 (32 bity)`
- `Windows 8.1 / IE 11.0.9600.18538`
- `Android 5.0.2 (wersja jądra 3.4.0+) / Chrome 52.0.2743.98`