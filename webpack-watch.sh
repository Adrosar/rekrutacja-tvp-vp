#!/bin/bash
cd $PWD
export PATH="$PWD:$PWD/node_modules/.bin:$PATH"
export webpack="$PWD/node_modules/.bin/webpack"
$webpack --debug --watch --config "$PWD/webpack.config.js"