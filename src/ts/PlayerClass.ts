import {ScreenClass} from "./ScreenClass";
import {ControlClass} from "./ControlClass";
import {ListClass} from "./ListClass";
import {appendStyle, createElementExt} from "./helpers";
import {RequestClass} from './RequestClass';


declare var require: any;

export class PlayerClass {

    private playerElement: HTMLDivElement;


    /*
     ToDo: Dodanie zastępczego odtwarzacza np: HLS https://github.com/videojs/videojs-contrib-hls

     Zamiast tworzyć obiekt:
     > this.screen = new ScreenClass(width, height);

     Można stworzyć nową klasę, która opakuje projekt HLS:
     > this.screen = new HlsClass(width, height);

     Klasa musi tylko posiadać pewne wymagane metody i mamy zastępczy odtwarzacz.
     To samo można zrobić z odtwarzaczem w technologii 'Adobe Flash' czy 'MS Silverlight'.

     */

    private screen: ScreenClass;
    private control: ControlClass;
    private list: ListClass;

    constructor(width: number, height: number) {
        appendStyle(require('../css/player.css'));

        this.screen = new ScreenClass(width, height);
        this.control = new ControlClass(this.screen);
        this.list = new ListClass(this.control, width);

        this.playerElement = createElementExt('div', {'class': 'vp-player'});

        let panelElement = createElementExt('div', {
            'class': 'vp-panel',
            'style': {
                'width': width + 'px',
                'height': height + 'px'
            }
        });

        panelElement.appendChild(this.screen.getScreenElement());
        panelElement.appendChild(this.control.getControlElement());

        this.playerElement.appendChild(panelElement);
        this.playerElement.appendChild(this.list.getListElement());

        appendStyle(require('../css/fullscreen.css'));
    }

    public getPlayerElement() {
        return this.playerElement;
    }

    public loadPlayList(url: string) {
        let req: RequestClass = new RequestClass();

        req.setCallback((status: number, _: any, data: string|any) => {
            if (status == 200) {
                this.list.genItems(<Array<any>>data);
            }
        });

        req.send('GET', url);
    }
}