import {ScreenClass} from "./ScreenClass";
import {appendStyle, fullScreen} from "./helpers";
import {createWidget, WidgetClass} from './widget';
import {UpdaterClass} from './UpdaterClass';

declare var require: any;

/**
 * Klasa kontrolera.
 */
export class ControlClass {

    private initURL: string;
    private updater: UpdaterClass;
    private widget: WidgetClass;
    private screen: ScreenClass;
    private status: number|null = null;
    private full: boolean = false;// FullScreen
    /*
     Wartości parametru 'status':
     ........ -1      | 0      | 1
     ........ stopped | paused | played
     */

    constructor(screen: ScreenClass) {
        appendStyle(require('../css/control.css'));

        this.screen = screen;

        // Tworzę widżet:
        this.widget = createWidget([null, 'div', {'class': 'vp-control'}, [
            [null, 'div', {'class': 'vp-row'}, [
                [null, 'div', {'class': 'vp-time'}, [
                    ['sliderTime', 'input', {'class': 'vp-input', 'type': 'range', 'name': 'points', 'min': '1', 'max': '100'}]
                ]]
            ]],
            [null, 'div', {'class': 'vp-row'}, [
                ['btnPlay', 'div', {'text': 'PLAY', 'class': 'vp-btn vp-play'}],
                ['btnPause', 'div', {'text': 'PAUSE', 'class': 'vp-btn vp-pause'}],
                ['btnStop', 'div', {'text': 'STOP', 'class': 'vp-btn vp-stop'}],
                [null, 'div', {'class': 'vp-volume'}, [
                    ['sliderVolume', 'input', {'class': 'vp-input', 'type': 'range', 'name': 'points', 'min': '0', 'max': '100', 'step': '10'}]
                ]],
                ['btnFullScr', 'div', {'text': 'FULL SCR', 'class': 'vp-btn vp-fullscr'}]
            ]]
        ]]);


        // Aktualizacja czasu odtwarzania:
        this.updater = new UpdaterClass(() => {

            let t = this.screen.getTime();
            let d = this.screen.getDuration();
            let r = 0;

            if (t > 0) {
                r = t / d * 100;
            } else {
                r = 0;
            }

            this.widget['sliderTime']._.value = r;
            //console.log(t, d, r);

            // Dodatkowo aktualizuje status kontrolki:
            this.setStatus(this.screen.getStatus());


        }, 200);

        // Aktualizuje suwak z czasem filmu:
        this.updater.fire();

        // Dodaje akcje dla przycisku FULL SCR
        this.widget['btnFullScr']._.addEventListener('click', () => {

            if (!!this.full) {
                //console.log('fullscreen false');
                fullScreen(false, this.getControlElement().parentNode);
                this.full = false;
            } else {
                //console.log('fullscreen true');
                fullScreen(true, this.getControlElement().parentNode);
                this.full = true;
            }

        });

        // Dodaje akcje dla przycisku PLAY:
        this.widget['btnPlay']._.addEventListener('click', () => {
            this.play();
        });

        // Dodaje akcje dla przycisku PAUSE:
        this.widget['btnPause']._.addEventListener('click', () => {
            this.pause();
        });

        // Dodaje akcje dla przycisku STOP:
        this.widget['btnStop']._.addEventListener('click', () => {
            this.stop();
        });

        // Sterowanie czasem odtwarzania:
        this.widget['sliderTime']._.addEventListener('mousedown', () => {
            this.updater.off();
            //console.log('sliderTime mousedown');
        });
        this.widget['sliderTime']._.addEventListener('change', () => {
            //console.log(this.widget['sliderTime']._.value);
            if (this.status >= 0) {
                this.screen.setTime(this.widget['sliderTime']._.value / 100 * this.screen.getDuration());
            }

        });
        this.widget['sliderTime']._.addEventListener('mouseup', () => {
            this.updater.on();
            //console.log('sliderTime mouseup');
        });

        // Ustawiam domyślny poziom głośności:
        //this.screen.setVolume(0.9);
        this.widget['sliderVolume']._.value = 90;


        // Sterowanie poziomem głośności:
        this.widget['sliderVolume']._.addEventListener('change', () => {
            //console.log(this.widget['sliderVolume']._.value);
            this.screen.setVolume(this.widget['sliderVolume']._.value / 100);
        });

    }

    /**
     * Metoda aktualizuje status kontrolki.
     * @param status: -1 | 0 | 1
     */
    public setStatus(status: number) {

        //console.log('setStatus', status);
        if (this.status != status) {
            switch (status) {
                case -1:// stopped
                    this.widget['btnStop']._.classList.add('vp-active');
                    this.widget['btnPause']._.classList.remove('vp-active');
                    this.widget['btnPlay']._.classList.remove('vp-active');
                    break;
                case 0:// paused
                    this.widget['btnStop']._.classList.remove('vp-active');
                    this.widget['btnPause']._.classList.add('vp-active');
                    this.widget['btnPlay']._.classList.remove('vp-active');
                    break;
                case 1:// played
                    this.widget['btnStop']._.classList.remove('vp-active');
                    this.widget['btnPause']._.classList.remove('vp-active');
                    this.widget['btnPlay']._.classList.add('vp-active');
                    break;
            }

            this.status = status;
        }

    }

    public getStatus() {
        return this.screen.getStatus();
    }

    public play(url?: string) {

        if (!!this.initURL && !url) {
            this.screen.play(this.initURL);
            this.initURL = null;
        } else {
            this.screen.play(url);
        }

        this.updater.on();

    }

    public pause() {
        this.updater.off();
        this.screen.pause();
        this.updater.fire();
    }

    public stop() {
        this.updater.off();
        this.screen.stop();
        this.updater.fire();
    }

    public getControlElement() {
        return this.widget.htmlElement;
    }

    /**
     * Metoda zwraca obecny czas odtwarzania filmu.
     * @returns {number} Czas w sekundach.
     */
    public getTime() {
        return this.screen.getTime();
    }

    public setPoster(url: string) {
        this.screen.setPoster(url);
    }

    public setInitURL(url: string) {
        this.initURL = url;
    }


}