import {setStyles, getDocument} from './helpers';

/**
 * Słownik :)
 */
interface Map<T> {
    [key: string]: T;
}

/**
 * Klasa dziecka.
 * @public htmlElement: Element HTML stworzony przez funkcję 'document.createElement'.
 * @public _: Alias do htmlElement.
 *
 */
export class WidgetClass {

    public _: any;
    public htmlElement: any;

    /**
     * Konstruktor klasy.
     * @param tag: Typ tagu HTML.
     * @param attributes: Słownik {nazwa: wartość} atrybutów HTML.
     */
    constructor(tag: string, attributes: any) {

        // Tworze element HTML:
        this._ = getDocument().createElement(tag);


        // Dodaję atrybuty do elementu HTML:
        if (!!attributes) {
            for (let key in attributes) {

                if (key == 'text') {

                    // Dodaję tekst do elementu.
                    this._.appendChild(getDocument().createTextNode(attributes[key]));

                } else if (key == 'style') {

                    // Dodaję style do elementu HTML:
                    setStyles(this._, attributes[key]);

                } else {

                    // Dodaje pozostałe atrybuty:
                    this._.setAttribute(key, attributes[key]);

                }

            }
        }

        this.htmlElement = this._;

    }
}

/**
 * Interfejs opisujący opcje jakie przyjmuje funkcja tworząca widżet.
 */
export interface WidgetOptions {
    [0]: string|null,// nazwa zmiennej (referencji)
    [1]: string,// nazwa tagu HTML
    [2]: Map<any|null>// atrybuty HTML
    [3]?: Array<WidgetOptions|null>// tablica z dziećmi

    /*
     Specjalne atrybuty HTML:
     - text: Wartość tekstowa tagu HTML, np: <div>Jakiś tekst</div>
     - style: Słownik {klucz: wartość} ze stylami CSS
     ... klucz przyjmuje nazwy styli CSS takich samych jak w plikach *.css
     ... wartość taka sama jak dla styli w plikach *.css
     */
}

/**
 * Funkcja tworzy dzieci które są instancjami klasy 'Widget'
 * @param root: Główny element.
 * @param parent: Rodzic.
 * @param attributes: Słownik {klucz: wartość} atrybutów HTML.
 * @param children: Lista [child, ...] dzieci.
 */
function createChilden(root: WidgetClass, parent: WidgetClass, attributes: Map<any|null>, children: Array<WidgetOptions|null>) {

    for (let key in children) {

        let child: WidgetClass = new WidgetClass(children[key][1], children[key][2]);

        if (!!children[key][0]) {
            root[children[key][0]] = child;
        }

        // DOM - Dodaję dziecko do rodzica
        parent._.appendChild(child._);

        // Tworze kolejne dzieci:
        if (!!children[key][3]) {
            createChilden(root, child, children[key][2], children[key][3]);
        }

    }

}

/**
 * Funkcja tworzy widżet z zagnieżdżonymi widżetami w środku.
 * @param options: Struktura na podstawie której jest tworzony widżet.
 * @returns {Widget}
 */
export function createWidget(options: WidgetOptions): WidgetClass {
    let root: WidgetClass = new WidgetClass(options[1], options[2]);

    if (!!options[0]) {
        // Tworze referencje do samego siebie:
        root[options[0]] = root;
    }

    if (!!options[3]) {
        // Tworzę dzieci:
        createChilden(root, root, options[2], options[3]);
    }

    return root;
}