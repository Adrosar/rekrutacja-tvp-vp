declare var XMLHttpRequest: any;
declare var ActiveXObject: any;

export interface ResponseInterface {
    (status: number, headers: {[key: string]: string}, data: string|any): void
}

export class RequestClass {
    private hrx: any;
    private callback: ResponseInterface;
    private data: string;
    private headers: {[key: string]: string};

    constructor() {
        this.headers = {};
        this.hrx = this.createHrx();
    }

    private createHrx() {
        let hrx: any;

        if (typeof XMLHttpRequest === "undefined") {
            hrx = new ActiveXObject(
                navigator.userAgent.indexOf("MSIE 5") >= 0 ? "Microsoft.XMLHTTP" : "Msxml2.XMLHTTP"
            );
        } else {
            hrx = new XMLHttpRequest();
        }

        return hrx;
    }

    public setHeader(name: string, value: string) {
        this.headers[name] = value;
    }

    public setData(data: string) {
        this.data = data;
    }

    public setCallback(callback: ResponseInterface) {
        this.callback = callback;
    }

    public send(type: string, url: string) {
        var t = this;
        this.hrx.open(type, url, true);

        if (!!t.callback) {
            this.hrx.onreadystatechange = function () {
                if (this.readyState == 4) {

                    // Generowanie nagłówków:
                    let headers: {[key: string]: string} = {};
                    let headers_rows: Object = (<string>t.hrx.getAllResponseHeaders()).split('\n');

                    for (let i in headers_rows) {

                        let row = (headers_rows[i] + "").trim();

                        if (row != '') {
                            let columns = row.split(":", 2);
                            headers[columns[0].trim()] = (columns[1] + "").trim();
                        }
                    }

                    // Generowanie treści odpowiedzi:
                    let data: string;

                    if (!!t.hrx.response) {
                        data = t.hrx.response;
                    } else if (!!t.hrx.responseText) {
                        data = t.hrx.responseText;
                    }

                    // Jeżeli odpowiedzią jest JSON to tworzę obiekt:
                    if (!!headers['Content-Type']) {
                        if (headers['Content-Type'].search('application/json') >= 0) {
                            if (!!data) {
                                data = JSON.parse(data);
                            }
                        }
                    }

                    t.callback(t.hrx.status, headers, data);
                }
            };
        }

        for (let key in this.headers) {
            this.hrx.setRequestHeader(key, this.headers[key]);
        }

        if (type == 'POST') {
            this.hrx.send(this.data);
        } else if (type == 'GET') {
            this.hrx.send();
        }
    }
}