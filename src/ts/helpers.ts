import {createWidget} from './widget';

/**
 * Funkcja sprawdza czy adres URL jest dostępny.
 * @param url: Adres URL.
 * @returns {boolean} Prawda lub fałsz.
 */
export function urlExists(url) {
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status != 404;
}

/**
 * Funkcja zwraca element 'head'.
 * @returns {HTMLHeadElement}
 */
export function getHead(): HTMLHeadElement {
    let d: any = getDocument();
    return d.head || d.getElementsByTagName('head')[0];
}

/**
 * Funkcja zwraca element 'body'
 * @returns {HTMLElement|HTMLBodyElement}
 */
export function getBody(): HTMLBodyElement|HTMLElement {
    let d: any = getDocument();
    return d.body || d.getElementsByTagName('body')[0];
}

/**
 * Funkcja zwraca obiekt widnow.document.
 * @returns {Document}
 */
export function getDocument(): HTMLDocument {
    return window.document;
}

/**
 * Funkcja dołącza do sekcji HEAD tag ze stylami.
 * @param rawCSS: Style CSS w postaci tekstowej.
 */
export function appendStyle(rawCSS: string) {
    let head: HTMLHeadElement = getHead();
    let style: HTMLStyleElement = getDocument().createElement('style');
    let text: Text = getDocument().createTextNode(rawCSS);

    style.type = 'text/css';

    style.appendChild(text);
    head.appendChild(style);
}

/**
 * Funkcja tworzy nowy element HTML na podstawie zadanych parametrów.
 * @param tagName: Typ elementu.
 * @param attributes: Słownik {klucz: wartość} parametrów.
 * @returns {any} Element HTML.
 */
export function createElementExt(tagName: string, attributes?: any): any {
    return createWidget([null, tagName, attributes, null])._;
}

/**
 * Funkcja ustawia style dla podanego elementu HTML.
 * @param element: Element HTML.
 * @param styles: Słownik {klucz: wartość} ze stylami.
 */
export function setStyles(element: any, styles: any) {

    if (!!element.style.setAttribute) {
        // IE
        for (let k in styles) {
            element.style.setAttribute(k, styles[k]);
        }

    } else {
        // inne
        let buffer: string = '';
        for (let k in styles) {
            buffer += k + ':' + styles[k] + ';'
        }

        element.style = buffer;

    }
}

/**
 * Funkcja ustawia tryb pełnego ekranu dla wybranego elementu HTML.
 * @param mode: true: Włącz pełny ekran | false: Wyłącz
 * @param element: Element HTML dla którego ma zostać włączony pełny ekran.
 */
export function fullScreen(mode: boolean, element: any): void {

    if (mode) {

        if (!!element.requestFullscreen) {
            element.requestFullscreen();
        } else if (!!element.msRequestFullscreen) {
            element.msRequestFullscreen();
        } else if (!!element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (!!element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else {
            console.log("Fullscreen API is not supported");
        }

    } else {

        let d: any = getDocument();

        if (!!d.exitFullscreen) {
            d.exitFullscreen();
        } else if (!!d.msExitFullscreen) {
            d.msExitFullscreen();
        } else if (!!d.mozCancelFullScreen) {
            d.mozCancelFullScreen();
        } else if (!!d.webkitCancelFullScreen) {
            d.webkitCancelFullScreen();
        }

    }

}

/**
 * Metoda sprawdza jakie formaty odtwarza dana przeglądarka.
 * ToDo: Można to zastosować do wyboru odpowiedniego pliku dla danej przeglądarki, a gdy żaden z formatów nie zostanie rozpoznany można wywołać zastępczy odtwarzacz wideo napisane w technologi 'Adobe Flash' lub 'MS Silverlight'.
 * @returns {{mpeg4: boolean, h264: boolean, ogg: boolean, webm: boolean}}
 */
export function checkCodecs() {
    let video = createElementExt("video");

    let mpeg4: boolean = false;
    let h264: boolean = false;
    let ogg: boolean = false;
    let webm: boolean = false;

    if (video.canPlayType) {
        // Check for MPEG-4 support
        mpeg4 = "" !== video.canPlayType('video/mp4; codecs="mp4v.20.8"');

        // Check for h264 support
        h264 = "" !== ( video.canPlayType('video/mp4; codecs="avc1.42E01E"')
            || video.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"') );

        // Check for Ogg support
        ogg = "" !== video.canPlayType('video/ogg; codecs="theora"');

        // Check for Webm support
        webm = "" !== video.canPlayType('video/webm; codecs="vp8, vorbis"');

    }

    return {
        'mpeg4': mpeg4,
        'h264': h264,
        'ogg': ogg,
        'webm': webm
    }
}

/**
 * Aliasy dla powyższych funkcji:
 */
export var cEX = createElementExt;

