module.exports = {
    entry: {
        'videoplayer': './src/ts/main.ts'
    },
    output: {
        filename: '[name].js',
        path: './dist'
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.d.ts', '.js']
    },
    module: {
        loaders: [
            {test: /\.ts$/, loader: 'ts-loader'},
            {test: /\.css$/, loader: 'css-raw-loader!postcss-loader'},
            {test: /\.html$/, loader: 'raw-loader'}
        ]
    }
};