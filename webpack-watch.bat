@ECHO OFF

SET MYPATH=%~dp0
CD %MYPATH%

SET "BINDIR=%CD%"
SET "WEBPACK=%CD%\node_modules\.bin\webpack.cmd"

%WEBPACK% --debug --watch --config "%CD%\webpack.config.js"
PAUSE